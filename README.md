# README #

This will set up a simple crud operation + routes inside your controller

### How do I get set up? ###

```
composer require lliure/simple-crud-router
```

or add the following to your composer.json

```
{
  "require": {
    "lliure/simple-crud-router": "*"
  }
}
```

### Usage example ###

```php
use Lliure\SimpleCrudRouter\RouteCreatorTrait;

class MyController extends Controller
{
    use RouteCreatorTrait;

    protected static $model = MyModel::class;
    protected static $routeGroup = '/app/mymodel';
}
```