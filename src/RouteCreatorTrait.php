<?php


namespace Lliure\SimpleCrudRouter;

use Lliure\Http\Message\ServerRequest;
use LliureCore\Collection;
use LliureCore\Model;
use Router\Router;

trait RouteCreatorTrait
{
    /**
     * @var string
     */
    protected static string $model;

    /**
     * @var string
     */
    protected static string $routeGroup;

    /**
     * @return Router
     */
    public static function router(): Router
    {
        $router = new Router();

        $router->group(static::$routeGroup, function() use ($router){
            $router->get("/list", [static::class, 'list']);
            $router->get("/{id}", [static::class, 'show']);
            $router->post("/", [static::class, 'create']);
            $router->put("/{id}", [static::class, 'update']);
            $router->delete("/{id}", [static::class, 'delete']);
        });

        return $router;
    }

    /**
     * @return Collection
     */
    public static function list(): Collection
    {
        $model = static::$model;

        return new Collection($model::all());
    }

    /**
     * @param int $id
     *
     * @return Model
     */
    public static function show(int $id): Model
    {
        $model = static::$model;

        return $model::load($id);
    }

    /**
     * @return Model
     */
    public static function create(): Model
    {
        $model = static::$model;
        $requestData = $model::filterFields(
            (array) ServerRequest::fromGlobals()->getParsedBody()
        );

        /* @var $modelObject Model */
        $modelObject = $model::build($requestData);
        $modelObject->insert();

        return $modelObject;
    }

    /**
     * @param int $id
     *
     * @return Model
     */
    public static function update(int $id): Model
    {
        $model = static::$model;
        $requestData = $model::filterFields(
            (array) ServerRequest::fromGlobals()->getParsedBody()
        );

        /* @var $modelObject Model */
        $modelObject = $model::find($id);

        foreach ($requestData as $field => $value) {
            $modelObject->$field = $value;
        }

        $modelObject->update();

        return $modelObject;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public static function delete(int $id): bool
    {
        $model = static::$model;

        /* @var $modelObject Model */
        $modelObject = $model::find($id);

        return $modelObject->delete();
    }
}
